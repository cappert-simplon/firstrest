package co.simplon.promo18.firstrest.controller;

import co.simplon.promo18.firstrest.entity.Operation;
import co.simplon.promo18.firstrest.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("api/operations")
@Validated
public class OperationController {

  @Autowired private OperationRepository repository;

  @GetMapping
  public List<Operation> all() {
    List<Operation> all = repository.findAll();
    return all;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Operation add(@RequestBody Operation operation) {
    return this.createPost(operation);
  }

  @GetMapping("/{id}")
  public Operation one(@PathVariable @Min(1) int id) {
    return this.showOne(id);
  }

  @PatchMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public Operation patch(@RequestBody Operation operation, @PathVariable @Min(1) int id) {
    Operation baseOp = repository.findById(id);
    if (baseOp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (operation.getLabel() != null) baseOp.setLabel(operation.getLabel());
    if (operation.getDate() != null) baseOp.setDate(operation.getDate());
    if (operation.getAmount() != 0.0) baseOp.setAmount(operation.getAmount());
    if (!repository.update(baseOp)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return baseOp;
  }

// pas d'id dans le GetMapping, car c'est une PathVariable
  @GetMapping("/show")
  public Operation show(@PathVariable @Min(1) int id) {
    return this.showOne(id);
  }

    //  Exercice RequestParam2 :
    //      1. Ajouter un Mapping sur /api/operation/add qui reçoit le label, la date, et le montant en RequestParam
    //      et insère l'opération correspondante dans la base
    //      2. Retourner un 201 Created si tout s'est bien passé
    //      3. Valider les paramètres avant insertion
  @PostMapping("/add")
  public Operation addWithParams(
      @RequestParam @NotBlank String label,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PastOrPresent LocalDate date,
      @RequestParam Double amount) {
        Operation o = new Operation(label, date, amount);
        return this.createPost(o);
  }

  private Operation createPost(Operation operation) {
    // si date, passer la LocalDate, sinon, créer la LocalDate à partir de TIME.NOW
    if (operation.getDate() == null) {
      operation.setDate(LocalDate.now());
    }
    repository.save(operation);
    return operation;
  }

  private Operation showOne(int id) {
    Operation op = repository.findById(id);
        if(op == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    return op;
  }


  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT) //Permet de renvoyer un code plus précis dans le cas d'un ajout réussi
  public void delete(@PathVariable @Min(1) int id) {
    if(!repository.delete(id)){
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/{id}")
  public Operation update(@Valid @RequestBody Operation operation, @PathVariable @Min(1) int id) {
    if (id != operation.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if(!repository.update(operation)){
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return repository.findById(operation.getId());
  }

}
