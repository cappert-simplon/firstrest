DROP TABLE IF EXISTS operation;

CREATE TABLE operation(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(128),
    date DATE NOT NULL,
    amount DOUBLE NOT NULL
) DEFAULT CHARSET UTF8;


INSERT INTO operation (label, date, amount) VALUES ("restaurant", "2022-05-01", -25),
("cloth", "2022-03-04", -100.24),
("groceries", "2022-04-28", -26.32),
("gift", "2022-01-14", 50);