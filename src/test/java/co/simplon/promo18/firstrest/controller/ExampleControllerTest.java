package co.simplon.promo18.firstrest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ExampleController.class)
public class ExampleControllerTest {

    @Autowired
    MockMvc mvc;

    /**
     * Dans cette méthode, on fait un test fonctionnel qui va simuler une requête http
     * vers une route de notre contrôleur. Ca permet de faire des tests à peu près 
     * similaire à ce que l'on ferait avec ThunderClient ou PostMan mais de manière
     * programmatique. (l'inconvénient c'est que c'est un peu plus long à écrire, 
     * l'avantage c'est qu'on a plus de contrôle et qu'on pourrait s'en servir pour
     * faire des tests sur une base de données de test dédiée, chose plus compliquée
     * à faire avec ThunderClient/PostMan)
     * @throws Exception
     */
    @Test
    void testFirstRoute() throws Exception {
        
        mvc.perform(
            post("/api/person") //On indique qu'on fait une requête post vers l'url donnée
            .contentType(MediaType.APPLICATION_JSON) //On indique que le contenu sera de type json
            .content(""" 
                {
                    "name": "Test",
                    "age": 46
                }
                """)) //On lui donne la string json qu'on veut envoyer vers la route
                .andExpect(
                    status().isOk() //On indique que le code http de retour doit être 200 "Ok"
                ).andExpect(
                    jsonPath("$.name").value("Test") //On indique que le json de retour doit contenir un name avec Test comme valeur
                );
    }

    @Test
    void testParamExample() {

    }

    @Test
    void testPostExample() {

    }
}
