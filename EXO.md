# REST avec Spring Boot

## Afficher une liste d'opération en JSON :
1. Créer une base de données p18_firstrest et dans celle ci, créer une table operation qui aura un id en integer, un label en varchar, une date en Date et un amount en double
2. Dans l'application Spring, créer une entité Operation dans un package entity (qui sera dans le package co.simplon.promo18.firstrest) et lui mettre les différentes propriétés avec juste la date en LocalDate, ses getter/setters et ses constructeurs
3. Dans le OperationRepository, créer ou modifier la méthode findAll pour faire qu'elle renvoie une List<Operation> et dedans s'inspirer des repo qu'on a déjà fait pour lui faire faire un findAll
4. Créer un nouveau contrôleur OperationController en vous inspirant du ExampleController, et dedans faire une méthode all() qui va utiliser le repository pour récupérer la liste des opération et la renvoyer (rajouter le OperationRepository en Autowired dans le contrôleur, commme on a fait pour la datasource) 

## Faire persister une Operation
1. Dans le repository, créer une méthode save(Operation operation) qui va faire persister une opération en bdd (""inspirez"" vous de ce qu'on a déjà fait avec le projet jdbc)
2. Dans le OperationController, créer une nouvelle méthode add (sur l'url /api/operation toujours) en vous inspirant de la méthode postExample du ExampleController qui va donc attendre en argument une Operation et qui va la faire persister à l'aide la méthode save() du repo
3. Utiliser ThunderClient pour vérifier si le post marche bien en lui faisant faire une requête de type POST vers http://localhost:8080/api/operation et en lui mettant une operation au format json dans le body

### Bonus :
* Faire que s'il y a une date, il la fasse persister telle quelle, mais que s'il y en a pas il met la date de maintenant
* Faire qu'on n'ait pas besoin de répéter l'url entre le GetMapping et le PostMapping, vu que c'est la même 

## Récupérer une opération par son id
1. Dans le OperationController, rajouter un nouveau GetMapping qu'on va appeler `one` et faire qu'il renvoie une Operation
2. S'inspirer du ExampleController pour faire que cette requête soit paramétrée et attende un id dans l'url
3. Utiliser cet id pour faire un findById et renvoyer l'operation récupérée
4. Rajouter une vérification qui fait que si le findById renvoie null, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException ) 

## Tests
En vous inspirant du testFindAll, faire un testFindById ainsi qu'un testSave
(Le testFindById ça sera presque exactement le même que le testFindAll, sauf que là il n'y aura même pas besoin des `assert` sur la `list` avant. Pour le `testSave`, il faudra créer une instance d'Operation, puis la donner à manger au `save` et par exemple vérifier si on a un id sur notre Operation)

## Supprimer une opération par son id
1. Dans le OperationController, rajouter un nouveau DeleteMapping
2. S'inspirer du ExampleController pour faire que cette requête soit paramétrée et attende un id dans l'url
3. Utiliser cet id pour faire un deleteById et supprimer l'opération récupérée
4. Rajouter une vérification qui fait que si le deleteById renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException )

## Modifier une opération par son id
1. Dans le OperationController, rajouter un nouveau PutMapping
2. S'inspirer du ExampleController pour faire que cette requête soit paramétrée et attende un id dans l'url
3. Utiliser cet id pour faire un update et mettre à jour l'opération récupérée
4. Rajouter une vérification qui fait que si le update renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException)

## Modifier partiellement une opération par son id
1. Dans le OperationController, rajouter un nouveau PatchMapping
2. S'inspirer du put pour faire que cette requête soit paramétrée et attendre un id dans l'url
3. Utiliser cet id pour faire un update et mettre à jour l'opération récupérée; attention, dans le patch, on ne reçoit que les champs à mettre à jour
4. Rajouter une vérification qui fait que si le update renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException)

## Validations :
1. Ajouter la dépendance Spring Validation à votre pom.xml
2. Ajouter les annotations à votre entité pour forcer l'utilisateur à fournir un label
3. Ajouter la validation dans le OperationController
4. Tester avec ThunderClient
5. Vérifier que la date fournie est dans le passé
6. Tester avec ThunderClient

## Valider l'id
rajouter le contrôle sur l'ID dans toutes les méthodes du contrôleur qui arrivent sur un `{id}`

## Exercice RequestParam :
  1. Ajouter un GetMapping sur `/api/operation/show` qui reçoit un id en RequestParameter et retourne l'opération correspondante
  2. Si l'opération n'existe pas, renvoyer une 404
  3. Si l'ID n'est pas valide, renvoyer une erreur
L'URL à tester sera donc `http://localhost:8080/api/operation/show?id=1`

## Exercice RequestParam2 : 
  1. Ajouter un Mapping sur /api/operation/add qui reçoit le label, la date, et le montant en RequestParam et insèrel'opération correspondante dans la base
  2. Retourner un 201 Created si tout s'est bien passé
  3. Valider les paramètres avant insertion
